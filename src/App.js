
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Layout from './layouts/Layout';
import Main from './pages/Main';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={ <Layout></Layout>}>
          <Route index element={ <Main></Main>}/>
          <Route path="weather">
            <Route index element={}/>
          </Route>

        </Route>

      </Routes>
    
    </BrowserRouter>
  );
}

export default App;
