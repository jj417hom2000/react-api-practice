import { useEffect, useState } from "react";
import { getMovieList } from "../api/WeatherAPI";
import MovieItem from "../components/WeatherItem";

function WeatherList() {

    const [weatherList, setWeatherList] = useState();

    useEffect(
        () => {
            getWeatherList()
                .then(data => setWeatherList(data));
        },
        []
    );

    console.log(movieList);

    return (
        <div className="content-row">
            { WeatherList && WeatherList.map(movie => <MovieItem key={ movie.movieCd } movie={ movie }/>) }
        </div>
    );
}

export default WeatherList;