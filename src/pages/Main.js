import { Link } from "react-router-dom";

function Main() {

    return (
        <Link to="/weather">
            <div className="content-row">
                <h1>이게 날씨?</h1>
            </div>
        </Link>
    );
}

export default Main;